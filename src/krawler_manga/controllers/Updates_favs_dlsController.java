/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import krawler_manga.Krawler_Manga;
import krawler_manga.core.krawler;
import krawler_manga.core.manga;

/**
 * FXML Controller class
 *
 * @author kinfinityIII
 */
public class Updates_favs_dlsController implements Initializable {
    private krawler k ; 
    @FXML
    public Label MODE;
    public TextField search_text;
    public FlowPane view;
    
    public void Exit(ActionEvent ev) throws IOException{
        if(null != Krawler_Manga.__MODE__)switch (Krawler_Manga.__MODE__) {
            case "SEARCH":
                Krawler_Manga._stage7.close();
                break;
            case "DOWNLOADS":
                Krawler_Manga._stage3.close();
                break;
            case "UPDATES":
                Krawler_Manga._stage6.close();
                break;
            case "FAVOURITES":
                Krawler_Manga._stage5.close();
                break;
            default:
                break;
        } 
    }
   
    public void Search (ActionEvent ev) throws IOException {}
    public void Filter (ActionEvent ev) throws IOException {}
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MODE.setText(Krawler_Manga.__MODE__);
        System.out.println(Krawler_Manga.__MODE__);
        Krawler_Manga._MANGA = new Stage();
        Krawler_Manga._MANGA.initStyle(StageStyle.UNDECORATED);
        if(null != Krawler_Manga.__MODE__)switch (Krawler_Manga.__MODE__) {
            case "SEARCH":
               search_text.setDisable(true);
               this._init_data(); 
               break;
            case "DOWNLOADS":
//               this.loadMangaFromDIsk(); 
                break;
            case "UPDATES":
                k = krawler.getInstance();
        {
            try {
                k.getsource();
            } catch (IOException ex) {
                Logger.getLogger(Updates_favs_dlsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                //search_text.setDisable(true);
                this.loadUpdatesFromKrawler();
                break;
            case "FAVOURITES":
                search_text.setDisable(true);
                this._init_data();
                break;
            case "DEFAULT":
                this._init_data();
                break;
        }
        ///set font color
    }    
    private void get_info(){}//data to be loaded depends on the mode... 
    //so we'd either crawl the web for the data or check the disk for cached or presaved data(downloaded)
    private void _init_data(){
        for(int i = 0;i<=20;i++){
            Label l = new Label("DEFAULT");
            ImageView iv = new ImageView(new Image(Krawler_Manga.class.getResource("images/testpic.jpg").toExternalForm()));
            l.setGraphic(iv);
            l.setPadding(new Insets(5,5,5,5));
            l.setContentDisplay(ContentDisplay.TOP);
            l.setTextAlignment(TextAlignment.CENTER);
            l.setOnMouseClicked((MouseEvent e) -> {
                try {
                   //open manga
                    System.out.println("openning manga");
                    Krawler_Manga._MANGA.close();
                    Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/manga.fxml"));
                    Scene sc = new Scene(root);
                    Krawler_Manga._MANGA.setScene(sc);
                    Krawler_Manga._MANGA.show();
                    Krawler_Manga._MANGA.setX(900);
                    Krawler_Manga._MANGA.setY(200);
                    System.out.println("reached here");
                    
                } catch (IOException ex) {
                    //catch exception
                }
            });
            view.setPadding(new Insets(5,5,5,20));
            view.getChildren().add(l);
        }
    }
    private void loadUpdatesFromKrawler(){
        int index = k.getimages().size();
        int count = index;
        for(Map.Entry<String,String> update : k.getupdates().entrySet()){
            Label l = new Label(update.getKey());
            ImageView iv = new ImageView(k.getimages().get(index-count));
            //iv.setImage(k.getimages().get(index));
            count--;
            l.setGraphic(iv);
            l.wrapTextProperty();
            l.setWrapText(true);
            l.setPadding(new Insets(5,5,5,5));
            l.setContentDisplay(ContentDisplay.TOP);
            l.setTextAlignment(TextAlignment.CENTER);
            l.setMaxSize(50, 100); // witdth / height
            l.setOnMouseClicked((MouseEvent e) -> {
                try {
                   //open manga
                    System.out.println("openning manga");
                    Krawler_Manga._MANGA.close();
                    Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/manga.fxml"));
                    Scene sc = new Scene(root);
                    Krawler_Manga._MANGA.setScene(sc);
                    Krawler_Manga._MANGA.show();
                    Krawler_Manga._MANGA.setX(900);
                    Krawler_Manga._MANGA.setY(200);
                    System.out.println("reached here");
                    
                } catch (IOException ex) {
                    //catch exception
                }
            });
            view.setPadding(new Insets(5,5,5,20));
            view.getChildren().add(l);
        }
    }
    
//    private void loadMangaFromDIsk(){
//        System.out.println("ok");
//        Krawler_Manga fsm = new Krawler_Manga();
//        for(manga currentmanga:fsm.loadedmanga_library){
//            System.out.println("1");
//            Label l = new Label(currentmanga.__MANGA_NAME__);
//            ImageView iv = new ImageView(currentmanga.__MANGA_IMAGE__);
//            System.out.println("2");
//            l.setGraphic(iv);
//            l.setPadding(new Insets(5,5,5,5));
//            l.setContentDisplay(ContentDisplay.TOP);
//            l.setTextAlignment(TextAlignment.CENTER);
//            l.setOnMouseClicked((MouseEvent e) -> {
//                try {
//                   //open manga
//                    System.out.println("openning manga");
//                    Krawler_Manga.__MANGA__ = currentmanga;
//                    Krawler_Manga._MANGA.close();
//                    Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/manga.fxml"));
//                    Scene sc = new Scene(root);
//                    Krawler_Manga._MANGA.setScene(sc);
//                    Krawler_Manga._MANGA.show();
//                    Krawler_Manga._MANGA.setX(900);
//                    Krawler_Manga._MANGA.setY(200);
//                    System.out.println("reached here");
//                    
//                } catch (IOException ex) {
//                    //catch exception
//                }
//            });
//            view.setPadding(new Insets(5,5,5,20));
//            view.getChildren().add(l);
//        }
//    }
}
