/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import krawler_manga.Krawler_Manga;

/**
 * FXML Controller class
 *
 * @author kinfinityIII
 */
public class MangaController implements Initializable {
    @FXML
    public ToggleButton favourite,download;
    public Button read,Exit;
    public Label manga_name;
    public ImageView manga_image;
    public TextArea description,summary;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        manga_name.setText(Krawler_Manga.__MANGA__.__MANGA_NAME__);
        manga_image.setImage(Krawler_Manga.__MANGA__.__MANGA_IMAGE__);
        description.setText("author \n year \n status \n genre");
        summary.setText("manga summary : \n Tale of the man who cut the sky");
        Image iv = new Image(Krawler_Manga.class.getResource("images/testpic.jpg").toExternalForm());
        manga_image.setImage(iv);
    }    
    
    public void Read (ActionEvent ev) throws IOException{}
    public void Favourite (ActionEvent ev) throws IOException{}
    public void Download (ActionEvent ev) throws IOException{}
    public void Exit(ActionEvent ev) throws IOException{
        Krawler_Manga._MANGA.close();
    }
}
