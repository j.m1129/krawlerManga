/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import krawler_manga.Krawler_Manga;

/**
 * FXML Controller class
 *
 * @author kinfinityIII
 */
public class SearchController implements Initializable {
    @FXML
    ToggleButton search;
    TextField search_text;
    ComboBox category;
    
    public void Exit(ActionEvent ev) throws IOException{
        Krawler_Manga._stage2.close();
    }
    public void Search(ActionEvent ev) throws IOException{
        //Search for the data / crawl the web by favs
        
        if(Krawler_Manga._stage7.isShowing()){
            Krawler_Manga._stage7.toFront();
        }else{
            Krawler_Manga._stage0.setY(Krawler_Manga.X_LEVEL_MAIN);
            Parent root = FXMLLoader.load(Krawler_Manga.class.getResource("fxml/updates_favs_dls.fxml"));
            Scene sc = new Scene(root);
            Krawler_Manga._stage7 = new Stage();
            Krawler_Manga._stage7.initStyle(StageStyle.UNDECORATED);
            Krawler_Manga._stage7.setScene(sc);
            Krawler_Manga._stage7.show();
            Krawler_Manga._stage7.setY(Krawler_Manga.Y_LEVEL_1);
        }
        
        if(null != Krawler_Manga.__MODE__)switch (Krawler_Manga.__MODE__) {
            case "SEARCH":
                Krawler_Manga._stage7.setX(Krawler_Manga.X_LEVEL_1);
                break;
            case "DOWNLOADS":
                Krawler_Manga._stage3.setX(Krawler_Manga.X_LEVEL_1);
                break;
            case "UPDATES":
                Krawler_Manga._stage6.setX(Krawler_Manga.X_LEVEL_1);
                break;
            case "FAVOURITES":
                Krawler_Manga._stage5.setX(Krawler_Manga.X_LEVEL_1);
                break;
            default:
                break;
        } 
        Krawler_Manga.__MODE__ = "SEARCH";
        Krawler_Manga._stage2.close();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // setup categories
    }    
    
}
