/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.util.List;

/**
 *
 * @author kinfinityIII
 */
public class List_Queue {
    private List<manga_description> manga_list;
    private List<manga_description> offline_list;
    private List<manga_description> updates_list;
    private List<manga_description> _list;
    private List<download> download_list;
    
    private List_Queue(){}
    public List_Queue get_Obj(){
        List_Queue obj = new List_Queue();
        return obj;
    }
    
    public List<manga_description> _getMangaList(){
        return manga_list;
    }
    public List<manga_description> _getOfflinelist(){
        return offline_list;
    }
    public List<manga_description> _getUpdates(){
        return updates_list;
    }
    public List<download> _getDownload(){
        return download_list;
    }

}
