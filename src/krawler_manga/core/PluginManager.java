/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import krawler_manga.Krawler_Manga;

/**
 *
 * @author kinfinityIII
 */
public class PluginManager {
    private static Krawler_Manga km;
    public static final ArrayList<mangaSource> sources = new ArrayList<>();
    public static String activeSource ;
    
    private PluginManager() {
    }
    
    public  void loadPlugin(String pluginClassName) {
        sources.forEach((mangaSource m)->{
            if(m.source_name.equalsIgnoreCase(pluginClassName)){
                //
            }else{
                try {
                    Class<?> pluginClass = getClass().getClassLoader().loadClass(pluginClassName);
                    krawlerMangaPlugin instance = (krawlerMangaPlugin) pluginClass.newInstance();
                    instance.attach(km,instance);
                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                    Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
    }
    //get and populate list from config .ini file
    public ArrayList<String> loadedPlugins(config cnfg){
        PluginManager.activeSource = cnfg.get_activeSource();
        return cnfg.sources;
    }
    
    //set active plugin or source
    public void set_activeSource(String sourceName){
        PluginManager.activeSource = sourceName;
    }
    
    //get instance
    public static PluginManager getInstance(Krawler_Manga ikm) {
        PluginManager.km = ikm;
        return PluginManagerHolder.INSTANCE;
    }
    
    private static class PluginManagerHolder {

        private static final PluginManager INSTANCE = new PluginManager();
    }
}
