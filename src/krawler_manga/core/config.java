/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.Set;
import org.ini4j.Ini;
import org.ini4j.Profile;

/**
 *
 * @author kinfinityIII
 */
public class config {
     
    public static class configHolder{
        public static final config instance = new config();
    }
    static config getInstance() {
        return configHolder.instance;
    }
    
    private Path main_dirPath;
    public String main_dir ;
    public ArrayList<String> sources;
    private String active_source = "";

    
    public String get_activeSource (){
        return this.active_source;
    }
    public void set_activeSource (String as){
        this.active_source = as;
    }
    public String get_library (){
        return this.active_source;
    }
    public void set_library (String libpath){
        this.active_source = libpath;
    }
    
        private config(){
            this.sources = new ArrayList<>();
           // this.main_dirPath = ;
        }
        
        public void load_config(File file) throws IOException,FileNotFoundException{
            Ini ini = new Ini();
            ini.load(new FileReader(file));
            Profile.Section defaults = ini.get("DEFAULTS");
            this.main_dir = defaults.get("MAIN_DIR");
            this.active_source= defaults.get("ACTIVE_SOURCE");
            
        }
        
        public void create_filesystem(){
//            this.main_dirPath = Paths.get(System.getProperty("user.home"),config.main_dir);
//            this.main_dirPath = Paths.get(config.main_dir);
            
            Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxr-x--");
            FileAttribute<Set<PosixFilePermission>> attr = PosixFilePermissions.asFileAttribute(perms);
//            Files.createDirectory(main_dirPath,attr);
        }
}
