/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.scene.image.Image;

/**
 *
 * @author kinfinityIII
 */
public class FileSystemManager {
    //create a filesystem for downloads
    // manage downloads search
    // organise files 
    // open files 
    public List<manga> loadedmanga_library;
    public ArrayList<Image> images = new ArrayList<>();
    public FileSystemManager(){}
    
    //arranges file names and chapters and images
    public void normalize_Library(){}
    
    //loads manga from Disk into an ObjectList
    public void load_fromDisk(){
        //3 levels of iteration -> home manga names, manga chapters , images from chapter
        //Path dir= Paths.get(config.main_dir);
        Path dir= Paths.get("D:\\SCNS\\MANGA WEBTOON");
        try {
            List<File> manga_folders = Files.walk(dir)
                    //.filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
            for(File manga_folder : manga_folders){
                manga current_manga = new manga();
                current_manga.__MANGA_NAME__ = manga_folder.toString();
                List<File> chapter_folders = Files.walk(manga_folder.toPath())
                    //.filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
                    //System.out.println(manga_folder.getAbsolutePath());
                    for(File manga_chapter : chapter_folders){
                        //System.out.println(manga_chapter.getCanonicalPath());
                        if(manga_chapter.isFile()){
                            //System.out.println("1 "+manga_chapter.toString());
                            current_manga.__MANGA_IMAGE__ = new Image(manga_chapter.getCanonicalPath());
                        }
                        //if(manga_chapter.isFile()){current_manga.__MANGA_IMAGE__ = new Image(Krawler_Manga.class.getResource(manga_chapter.getCanonicalPath()).toString());}
                        List<File> chapter_images = Files.walk(manga_chapter.toPath())
                                .filter(Files::isRegularFile)
                                .map(Path::toFile)
                                .collect(Collectors.toList());
                        chapter_images.stream().distinct().forEach((File image) -> {
                        try {
                            if(image!=null){  
                                System.out.println(image.getCanonicalPath());
                                this.images.add(new Image(image.getCanonicalPath()));
                                System.out.println("LOADED");
                            }else{
                                System.out.println("didn't load the picture");
                            }
                        }catch (IOException ex) {
                            Logger.getLogger(FileSystemManager.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        });
                        
                        try{
                            current_manga.chapters.put(manga_chapter.toString(),images );
                        }catch(Exception e){}
                        this.images.clear();
                    }
            }
        }catch(IOException | DirectoryIteratorException x){
            System.err.println(x);
        }
    }
}
