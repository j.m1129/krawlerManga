/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author kinfinityIII
 */
public class krawler {
    
    private final Map<String,String> updatelinks;//name and link
    private manga_description currentdescription;
    private final ArrayList<Image> imlist = new ArrayList<>();
    
    //connect to source
    public void getsource() throws IOException,FileNotFoundException{
        //Document source_page = Jsoup.connect(config.active_source).get();
       Document source_page = Jsoup.connect("http://mangasim.com/").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11").timeout(50000).get();
       String title = source_page.title();
        //System.out.println(source_page.toString());
        Elements content = source_page.getElementsByClass("manga_item");
        System.out.println(Arrays.toString(content.toArray()));
        content.stream().distinct().forEach((Element link) -> {
            this.currentdescription = new manga_description();
            krawler.this.updatelinks.put(link.getElementsByClass("manga_title").first().child(0).text(),link.getElementsByClass("manga_title").first().child(0).attr("href"));
            //System.out.println(krawler.this.links);
            //System.out.println(link.text());
            System.out.println(link.getElementsByClass("manga_title").first().child(0).attr("href"));
            try {
                URLConnection urlcon = new URL(link.getElementsByClass("manga_img").first().getElementsByTag("img").first().attr("abs:src")).openConnection();
                urlcon.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                urlcon.connect();
                InputStream urlstream = urlcon.getInputStream();
                if(!urlstream.equals((Object)null)){
                    imlist.add(new Image(urlstream));
                    //imlist.add(new Image(ImageIO.read(new URL (temp.attr("abs:src"))).toString()));
                }else{}
                
            } catch (IOException ex) {
                Logger.getLogger(krawler.class.getName()).log(Level.SEVERE, null, ex);
            } 
        });
        System.out.println(krawler.this.updatelinks.toString());
        //gotten all links on page... 
        //filter links
//        links.forEach((Map<String,String> current_link)->{
//            if(!current_link.containsKey("")){ links.remove("");}
//        });
        
        
    }
    public Map<String,String> getupdates(){return this.updatelinks;}
    public ArrayList<Image> getimages(){return this.imlist;}
    
    private krawler() {
        this.updatelinks = new LinkedHashMap<>();
    }
    
    public static krawler getInstance() {
        return krawlerHolder.INSTANCE;
    }
    
    private static class krawlerHolder {

        private static final krawler INSTANCE = new krawler();
    }
}
