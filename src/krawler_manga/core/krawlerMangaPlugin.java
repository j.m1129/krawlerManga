/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.util.ArrayList;
import krawler_manga.Krawler_Manga;

/**
 *
 * @author kinfinityIII
 */
public interface krawlerMangaPlugin {
    //public void load(PluginConfiguration plugConf);
    public default void attach(Krawler_Manga app,krawlerMangaPlugin x){
        System.out.println(x.toString()+" Plugin Connected");
    }
    public void unload();
    public void run();
    public void get_info();
    public ArrayList<manga_description> update_List();
    public ArrayList<manga_description> update_AllAZ();
}
