/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import krawler_manga.Krawler_Manga;

/**
 *
 * @author kinfinityIII
 */
public class __CORE__ implements Runnable {
    
    static Krawler_Manga app_instance; 
    private __CORE__() {
    }
    
    
    
    public static __CORE__ getInstance(Krawler_Manga instance) {
        __CORE__.app_instance = instance;
        return __CORE__Holder.INSTANCE;
    }

    @Override
    public void run() {
        try {
            ///// startup program
            config config_setting =config.getInstance();
            String config_source = "";
            config_setting.load_config(new File(config_source));
            config_setting.create_filesystem();
            PluginManager plugs_m = PluginManager.getInstance(__CORE__.app_instance);
//          
//            plugs_m.loadedPlugins(config_setting).forEach((String source)->{
//                plugs_m.loadPlugin(source);
//            });
//            
            //create objects for each source
            sourceFactory sF = sourceFactory.getInstance();
            sF.create_instance(config_setting.sources);
            
            
            FileSystemManager fsm = new FileSystemManager();
            //read config .ini file
            //get sources
            //get Library path
            //
            
            //create or load filesystem
            
            //check and load downloaded files
            
            //connect to and krawl active source
        } catch (IOException ex) {
            Logger.getLogger(__CORE__.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static class __CORE__Holder {

        private static final __CORE__ INSTANCE = new __CORE__();
    }
    public static Map<String,mangaSource> source_objects = new HashMap();
    private static class sourceFactory{
        private sourceFactory(){}
        public void create_instance(ArrayList<String> sources){
            sources.forEach((String s)->{
                try {
                    Class<?> source = getClass().getClassLoader().loadClass(s);
                     __CORE__.source_objects.put(s,(mangaSource) source.newInstance());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                    Logger.getLogger(__CORE__.class.getName()).log(Level.SEVERE, null, ex);
                }
                   
            });
        }
        private static class sourceFactoryHolder{
            public static final sourceFactory instance = new sourceFactory();
        }
        public static sourceFactory getInstance(){
            return sourceFactoryHolder.instance;
        }
    }
}
