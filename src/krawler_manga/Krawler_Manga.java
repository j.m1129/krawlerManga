/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package krawler_manga;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import krawler_manga.core.FileSystemManager;
import krawler_manga.core.__CORE__;
import krawler_manga.core.manga;

/**
 *
 * @author kinfinityIII
 */
public class Krawler_Manga extends Application {
    public static Stage _stage0,_stage1,_stage2,_stage3,_stage4,_stage5,_stage6,_stage7,_MANGA;
    public static int X_LEVEL_MAIN=10;
    public static int Y_LEVEL_MAIN=10;
    public static int X_LEVEL_1=100;
    public static int X_LEVEL_2=200;
    public static int Y_LEVEL_1=150;
    public static int Y_LEVEL_2=200;
    public static String __MODE__ = "DEFAULT";
    public static manga __MANGA__ ;
    

 
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxml/main.fxml"));
        
        Scene scene = new Scene(root);
        Krawler_Manga._stage0 = new Stage();
        Krawler_Manga._stage0.setScene(scene);
        Krawler_Manga._stage0.initStyle(StageStyle.UNDECORATED);
        Krawler_Manga._stage0.show();
        //Krawler_Manga._stage0.setAlwaysOnTop(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        launch(args);
        __CORE__ c = __CORE__.getInstance(new Krawler_Manga());
        c.run();
    }
    
   
}
